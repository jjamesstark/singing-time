---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'He Sent His Son (S34)'
footer: 'Level 2'
---

# He Sent His Son
### Songbook 34

---

How c____ the Father t___ the world
 of l___ and t_________?

He sent his Son, a n______ b___, 
  with p____ and holiness.

---

How could the Father s___ the w____
 the p______ we s_____ go?

He sent his Son to w___ with m__
 on e____, that we may k___.

---

How could the F_____ t___ the world
 of s________, of d____?

He sent his Son to d__ for u_
 and r___ with l_____ breath.

---

What does the F_____ a__ of us?
 What do the s_________ say?

Have f____, have h___, live like his Son,
 help o_____ on their w__.

---

What d___ he a__?
...
Live l___ his S__.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->