---
marp: true
style: |
  {
    font-size: 1.5rem;
  }
header: 'We Are Different (S263)'
footer: 'pattern'
backgroundColor: deepskyblue
---

# We Are Different 
## _(songbook 263)_

---

| I * you | 2nd Line                                             |
|---------|------------------------------------------------------|
| **know* | _We are as diff’rent as the sun and the sea._        |
| **help* | _We learn from problems, and we’re starting to see._ |
| **love* | _We reach together for the best we can be._          |

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->