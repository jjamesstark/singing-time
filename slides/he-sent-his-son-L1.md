---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'He Sent His Son (S34)'
footer: 'Level 1'
---

# He Sent His Son
### Songbook 34

---

How could the Father t___ the world
 of love and t_________?

He sent his Son, a n______ babe, 
  with p____ and holiness.

---

How could the Father s___ the world
 the p______ we should go?

He sent his Son to w___ with men
 on earth, that we may k___.

---

How could the F_____ tell the world
 of s________, of death?

He sent his Son to d__ for us
 and r___ with living breath.

---

What does the Father a__ of us?
 What do the s_________ say?

Have faith, have h___, live like his Son,
 help o_____ on their way.

---

What d___ he ask?
...
Live l___ his Son.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->