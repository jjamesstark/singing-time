---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'A Child’s Prayer (S12)'
footer: 'Level 1'
---
# A Child’s Prayer
## (Songbook 12)

---
<!-- header: A Child’s Prayer (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Heavenly Father, are you r_____ there?

And do you hear and a_____ ev’ry child’s prayer?

Some say that h_____ is far away,

But I feel it c____ around me as I pray.

---
<!-- header: A Child’s Prayer (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Heavenly Father, I r_______ now

Something that Jesus told d________ long ago:

“Suffer the c_______ to come to me.”

Father, in p_____ I’m coming now to thee.

---
<!-- header: A Child’s Prayer (Verse 2)-->
<!-- backgroundColor: LemonChiffon -->

P___, he is there;

S____, he is list’ning.

Y__ are his child;

His l___ now surrounds you.

---
<!-- header: A Child’s Prayer (Verse 2)-->
<!-- backgroundColor: LemonChiffon -->

He h____ your prayer;

He l____ the children.

Of such is the k______, the kingdom of heav’n.

---
<!-- header: A Child’s Prayer -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->