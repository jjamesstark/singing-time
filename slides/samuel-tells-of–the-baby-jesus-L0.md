---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Samuel Tells of the Baby Jesus (S36)'
footer: 'Level 0'
---

# Samuel Tells of the Baby Jesus
## Songbook 36

---
<!-- header: Samuel Tells of the Baby Jesus (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Said Samuel, “Within five years

A night will be as day,

And Baby Jesus will be born

In a land far, far away.”

---
<!-- header: Samuel Tells of the Baby Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon -->

Hosanna! Hosanna!

Oh, let us gladly sing.

How blessed that our Lord was born;

Let earth receive her King!

---
<!-- header: Samuel Tells of the Baby Jesus (Verse 2)-->
<!-- backgroundColor: LightPink -->

Across the sea, in Bethlehem,

Lord Jesus came to earth

As Samuel had prophesied,

And angels sang His birth.

---
<!-- header: Samuel Tells of the Baby Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon -->


Hosanna! Hosanna!

Oh, let us gladly sing.

How blessed that our Lord was born;

Let earth receive her King!

---
<!-- header: Samuel Tells of the Baby Jesus -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->