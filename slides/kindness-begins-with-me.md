---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Kindness Begins with Me  (S145b)'
footer: 'Level 0'
backgroundColor: deepskyblue
---

# Kindness Begins with Me 
## _(Songbook 145b)_

---

I want to be kind to ev’ryone,

  For that is right, you see.

So I say to myself, “Remember this:

  Kindness begins with me.”

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->