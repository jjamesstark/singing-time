---
marp: true
style: |
  {
    font-size: 1.5rem;
  }
header: 'Stars Were Gleaming (S37)'
backgroundColor: Snow
---

# Stars Were Gleaming
## _(songbook 37)_

---
<!-- footer: Verse 1-->
<!-- backgroundColor: SeaGreen -->

Stars were gleaming, shepherds dreaming;
And the night was dark and chill.

Angels’ story rang with glory;
Shepherds heard it on the hill.

---

Ah, that singing! Hear it ringing,
Earthward winging, Christmas bringing!

Hearken! We can hear it still!

---
<!-- footer: Verse 2-->
<!-- backgroundColor: FireBrick -->
<!-- color: White -->

See the clearness and the nearness
Of the blessed Christmas star,

Leading, guiding; wise men riding
Through the desert dark and far.

---

Lovely showing, shining, growing,
Onward going, gleaming, glowing,

Leading still, our Christmas star!

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->