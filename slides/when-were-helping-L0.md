---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: '👷🏻‍♀️ When We’re Helping (198b)'
footer: 'Level 0'
backgroundColor: deepskyblue
---

# 👷🏻‍♀️ When We’re Helping 
### Songbook 198b

---

When we’re helping, we’re happy,

And we sing as we go;

And we like to help each-other,

So that our love will show.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->