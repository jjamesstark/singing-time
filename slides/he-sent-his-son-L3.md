---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'He Sent His Son (S34)'
footer: 'Level 3'
---

# He Sent His Son
### Songbook 34

---

H c t F **tell** 🌏; of love and tenderness?
H s h S, a newborn babe; with peace and holiness.
H c t F **show** 🌏; the pathway we should go
H s h S **to walk with men**; on earth, that we may know
H c t F **tell** 🌏; of sacrifice, of death?
H s h S **to die for us**; and rise with living breath.

---

What does the F_____ a__ of us?
 What do the s_________ say?

Have f____, have h___, l___ like h__ S__,
 help o_____ on their w__.

What d___ he a__?
Live l___ h__ S__.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->