---
marp: true
style: |
  {
    font-size: 1.5rem;
  }
header: 'I’m Trying to Be like Jesus (S78)'
footer: 'Level 0'
---

# I’m Trying to Be like Jesus
## Songbook 78

---
<!-- header: I’m Trying to Be like Jesus (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

I’m trying to be like Jesus;

I’m following in his ways.

I’m trying to love as he did, in all that I do and say.

At times I am tempted to make a wrong choice,

But I try to listen as the still small voice whispers,

---
<!-- header: I’m Trying to Be like Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon   -->

“Love one another as Jesus loves you.

Try to show kindness in all that you do.

Be gentle and loving in deed and in thought,

For these are the things Jesus taught.”

---
<!-- header: I’m Trying to Be like Jesus (Verse 2)-->
<!-- backgroundColor: LightPink -->

I’m trying to love my neighbor;

I’m learning to serve my friends.

I watch for the day of gladness when Jesus will come again.

I try to remember the lessons he taught.

Then the Holy Spirit enters into my thoughts, saying:

---
<!-- header: I’m Trying to Be like Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon   -->

“Love one another as Jesus loves you.

Try to show kindness in all that you do.

Be gentle and loving in deed and in thought,

For these are the things Jesus taught.”

---
<!-- header: I’m Trying to Be like Jesus -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->
