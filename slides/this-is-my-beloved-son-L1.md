---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: "This Is My Beloved Son (S76)"

---

# This Is My Beloved Son
## Songbook 76

---
<!-- footer: Verse 1 -->
<!-- backgroundColor: deepskyblue -->

Jesus entered Jordan’s ______
When His ____ had just begun.
God the Father spoke from ______:
“This is My _______ Son. Hear Him!”

---
<!-- footer: Verse 2 -->
<!-- backgroundColor: LemonChiffon -->

Nephites ______ into heaven
Saw their white-robed ______ come.
And they heard the Father _______:
“This is My _______ Son. ____ Him!”

---
<!-- footer: Verse 3 -->
<!-- backgroundColor: Orange -->

Joseph saw two ________ beings
_______ brighter than the sun.
God again _________ Jesus:
“This is __ _______ Son. ____ ___!”

---
<!-- footer: Verse 4 -->
<!-- backgroundColor: PaleVioletRed  -->

As I read the __________ daily—
Words of Christ, the ____ One—
In my _____ I’ll hear God tell me:
“____ is __ _______ ___. ____ ___!”

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->