---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: '“Give,” said the little stream, (S236)'
footer: 'Level 1'
---

# "Give," Said the Little Stream
## Songbook 236

---
<!-- header: "'Give,' Said the Little Stream (Verse 1)" -->
<!-- backgroundColor: deepskyblue -->

“____,” said the ______ stream,
“____, oh! ____, ____, oh! ____.”
“____,” said the little ______,
As it _______ down the hill;
“I’m _____, I know, but wherever I go
The f_____ grow greener still.”

---
<!-- header: "'Give,' Said the Little Stream (Chorus)" -->
<!-- backgroundColor: LemonChiffon -->

Singing, singing all the ___,
“Give away, oh! give away.”
Singing, singing all the ___,
“Give, oh! give away.”

---
<!-- header: "'Give,' Said the Little Stream (Verse 2)" -->
<!-- backgroundColor: LightSalmon -->

“Give,” said the little ____,
“Give, oh! give, give, oh! give.”
“Give,” said the little ____,
As it ____ upon the flow’rs;
“I’ll raise their ________ heads again,”
As it ____ upon the flow’rs.

---
<!-- header: "'Give,' Said the Little Stream (Chorus)" -->
<!-- backgroundColor: LemonChiffon -->

Singing, singing all the day,
“Give away, oh! give away.”
Singing, singing all the day,
“Give, oh! give away.”

---
<!-- header: "'Give,' Said the Little Stream (Verse 3)" -->
<!-- backgroundColor: MediumSpringGreen -->

Give, then, as _____ gives,
Give, oh! give, give, oh! give.
Give, then, as _____ gives;
There is _________ all can give.
Do as the _______ and ________ do:
For God and ______ live.

---
<!-- header: "'Give,' Said the Little Stream (Chorus)" -->
<!-- backgroundColor: LemonChiffon -->

Singing, singing all the day,
“Give away, oh! give away.”
Singing, singing all the day,
“Give, oh! give away.”

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->