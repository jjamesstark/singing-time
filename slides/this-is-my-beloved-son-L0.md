---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: "This Is My Beloved Son (S76)"

---

# This Is My Beloved Son
## Songbook 76

---
<!-- footer: Verse 1 -->
<!-- backgroundColor: deepskyblue -->

Jesus entered Jordan’s waters
When His work had just begun.
God the Father spoke from heaven:
“This is My Beloved Son. Hear Him!”

---
<!-- footer: Verse 2 -->
<!-- backgroundColor: LemonChiffon -->

Nephites gazing into heaven
Saw their white-robed Savior come.
And they heard the Father witness:
“This is My Beloved Son. Hear Him!”

---
<!-- footer: Verse 3 -->
<!-- backgroundColor: Orange -->

Joseph saw two glorious beings
Shining brighter than the sun.
God again presented Jesus:
“This is My Beloved Son. Hear Him!”

---
<!-- footer: Verse 4 -->
<!-- backgroundColor: PaleVioletRed  -->

As I read the scriptures daily—
Words of Christ, the Holy One—
In my heart I’ll hear God tell me:
“This is My Beloved Son. Hear Him!”

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->