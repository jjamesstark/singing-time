---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Christmas Bells Are Ringing (S54)'

---
# Christmas Bells Are Ringing
## (Songbook 54)

---
<!-- backgroundColor: SeaGreen -->

Christmas bells are ringing.
Hear what they say to you:
Jesus is born in Bethlehem
in Bethlehem.

---
<!-- backgroundColor: FireBrick -->
<!-- color: White -->

Christmas bells, ringing, singing:
Jesus is born
is born in Bethlehem
born in Bethlehem.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->