---
marp: true
style: |
  {
    font-size: 1.4rem;
  }
header: 'He Sent His Son (S34)'
footer: 'Pattern'
---

# He Sent His Son
### Songbook 34

---

|Start      | end  |
|-----------|------|
| How could |  **tell** 🌏 ; of love and tenderness? |
| He sent   | **a newborn babe**; with peace and holiness. |
| How could |  **show** 🌏; the pathway we should go |
| He sent   | **to walk with men**; on earth, that we may know |
| How could | **tell** 🌏; of sacrifice, of death? |
| He sent   | **to die for us**; and rise with living breath. |

---

W D T F A O U
 W D T S S?

H F H H, L L H S
 H O O T W

W D H A
L L H S

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->