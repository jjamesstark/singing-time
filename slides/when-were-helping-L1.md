---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: '👷🏻‍♀️ When We’re Helping (198b)'
footer: 'Level 1'
backgroundColor: deepskyblue
---

# 👷🏻‍♀️ When We’re Helping 
### Songbook 198b

---

When we’re h______, we’re h____,

And we s___ as we go;

And we like to h___ each-other,

So that our l___ will show.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->