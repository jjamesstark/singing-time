---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: '👷🏻‍♀️ When We’re Helping (198b)'
footer: 'Pattern'
backgroundColor: deepskyblue
---

# 👷🏻‍♀️ When We’re Helping 
### Songbook 198b

---

W w h w h

A w s a w g

A w l t h e o

S t o l w s

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->