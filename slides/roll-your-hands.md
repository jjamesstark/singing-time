---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Roll Your Hands (S274)'

---
# Christmas Bells Are Ringing
## (Songbook 54)

---
<!-- backgroundColor: Aqua -->

Roll your hands, roll your hands,
##### As slowly*, as slowly* as slow can be.

Then fold your arms like me, like me,
Then fold your arms like me.

###### _*swiftly_

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->