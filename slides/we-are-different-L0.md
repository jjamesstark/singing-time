---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'We Are Different (S263)'
footer: 'Level 0'
---

# We Are Different 
## _(songbook 263)_

---

<!-- header: We Are Different (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

I **know** you, and you know me.

_We are as diff’rent as the sun and the sea._

I know you, and you know me,

And that’s the way it’s supposed to be.

---

<!-- header: We Are Different (Verse 2)-->
<!-- backgroundColor: LightPink -->

I **help** you, and you help me.

_We learn from problems, and we’re starting to see._

I help you, and you help me,

And that’s the way it’s supposed to be.

---

<!-- header: We Are Different (Verse 3)-->
<!-- backgroundColor: LemonChiffon -->

I **love** you, and you love me.

_We reach together for the best we can be._

I love you, and you love me,

And that’s the way it’s supposed to be.

---
<!-- header: We Are Different -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->