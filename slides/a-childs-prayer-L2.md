---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'A Child’s Prayer (S12)'
footer: 'Level 2'
---
# A Child’s Prayer
## (Songbook 12)

---
<!-- header: A Child’s Prayer (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

H_______ F_____, are you r_____ there?

And do you h___ and a_____ ev’ry child’s p_____?

S___ say that h_____ is far away,

But I feel it c____ a_____ me as I p___.

---
<!-- header: A Child’s Prayer (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

H_______ F_____, I r_______ now

S________ that Jesus told d________ long ago:

“S_____ the c_______ to come to me.”

F_____, in p_____ I’m c_____ now to thee.

---
<!-- header: A Child’s Prayer (Verse 2)-->
<!-- backgroundColor: LemonChiffon -->

P___, he is t____;

S____, he is l________.

Y__ are his c____;

His l___ now s________ you.

---
<!-- header: A Child’s Prayer (Verse 2)-->
<!-- backgroundColor: LemonChiffon -->

He h____ your p_____;

He l____ the c_______.

Of such is the k______, the k______ of h_____.

---
<!-- header: A Child’s Prayer -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->