---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'He Sent His Son (S34)'
footer: 'Level 0'
---

# He Sent His Son
### Songbook 34

---

How could the Father tell the world
 of love and tenderness?

He sent his Son, a newborn babe, 
  with peace and holiness.

---

How could the Father show the world
 the pathway we should go?

He sent his Son to walk with men
 on earth, that we may know.

---

How could the Father tell the world
 of sacrifice, of death?

He sent his Son to die for us
 and rise with living breath.

---

What does the Father ask of us?
 What do the scriptures say?

Have faith, have hope, live like his Son,
 help others on their way.

---

What does he ask?
...
Live like his Son.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->