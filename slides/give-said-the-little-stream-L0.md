---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: '“Give,” said the little stream, (S236)'
footer: 'Level 0'
---

# "Give," Said the Little Stream
## Songbook 236

---
<!-- header: "'Give,' Said the Little Stream (Verse 1)" -->
<!-- backgroundColor: deepskyblue -->

“Give,” said the little stream,
“Give, oh! give, give, oh! give.”
“Give,” said the little stream,
As it hurried down the hill;
“I’m small, I know, but wherever I go
The fields grow greener still.”

---
<!-- header: "'Give,' Said the Little Stream (Chorus)" -->
<!-- backgroundColor: LemonChiffon -->

Singing, singing all the day,
“Give away, oh! give away.”
Singing, singing all the day,
“Give, oh! give away.”

---
<!-- header: "'Give,' Said the Little Stream (Verse 2)" -->
<!-- backgroundColor: LightSalmon -->

“Give,” said the little rain,
“Give, oh! give, give, oh! give.”
“Give,” said the little rain,
As it fell upon the flow’rs;
“I’ll raise their drooping heads again,”
As it fell upon the flow’rs.

---
<!-- header: "'Give,' Said the Little Stream (Chorus)" -->
<!-- backgroundColor: LemonChiffon -->

Singing, singing all the day,
“Give away, oh! give away.”
Singing, singing all the day,
“Give, oh! give away.”

---
<!-- header: "'Give,' Said the Little Stream (Verse 3)" -->
<!-- backgroundColor: MediumSpringGreen -->

Give, then, as Jesus gives,
Give, oh! give, give, oh! give.
Give, then, as Jesus gives;
There is something all can give.
Do as the streams and blossoms do:
For God and others live.

---
<!-- header: "'Give,' Said the Little Stream (Chorus)" -->
<!-- backgroundColor: LemonChiffon -->

Singing, singing all the day,
“Give away, oh! give away.”
Singing, singing all the day,
“Give, oh! give away.”

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->