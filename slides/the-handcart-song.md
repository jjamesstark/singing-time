---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'The Handcart Song (S220)'
---
# The Handcart Song
## (Songbook 220)

---
<!-- backgroundColor: LemonChiffon -->
<!-- footer: verse-->
When pioneers moved to the West,

With courage strong they met the test.

They pushed their handcarts all day long,

And as they pushed they sang this song:

---
<!-- backgroundColor: deepskyblue -->
For some must push and some must pull,

As we go marching up the hill;

So merrily on our way we go

Until we reach the Valley-o.

---

<!-- backgroundColor: Black -->

---
<!-- backgroundColor: lightcoral -->
<!-- footer: descant-->
Push and pull
As we go up the hill;
So on our way
Until we reach the Valley-o.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->