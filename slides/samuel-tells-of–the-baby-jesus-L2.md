---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Samuel Tells of the Baby Jesus (S36)'
footer: 'Level 2'
---

# Samuel Tells of the Baby Jesus
## Songbook 36

---
<!-- header: Samuel Tells of the Baby Jesus (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Said Samuel, “Within f___ years

A n____ will be as d__,

And Baby J____ will be b___

In a l___ far, far a___.”

---
<!-- header: Samuel Tells of the Baby Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon -->

Hosanna! Hosanna!

Oh, l__ us g_____ sing.

How b______ that our Lord was b___;

Let earth r______ her King!

---
<!-- header: Samuel Tells of the Baby Jesus (Verse 2)-->
<!-- backgroundColor: LightPink -->

A_____ the sea, in B________,

Lord Jesus c___ to e____

As S_____ had p_________,

And a_____ sang His b____.

---
<!-- header: Samuel Tells of the Baby Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon -->


H______! H______!

Oh, let us g_____ s___.

How b______ that our L___ was b___;

Let e____ r______ her K___!

---
<!-- header: Samuel Tells of the Baby Jesus -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->