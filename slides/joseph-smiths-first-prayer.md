---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: "Joseph Smith's First Prayer (H26)"

---

# Joseph Smith's First Prayer
## Hymnal 26

---
<!-- footer: Verse 1 -->
<!-- backgroundColor: deepskyblue -->

Oh, how lovely was the morning!
Radiant beamed the sun above.
Bees were humming, sweet birds singing,
Music ringing thru the grove,

---

When within the shady woodland
Joseph sought the God of love,
When within the shady woodland
Joseph sought the God of love.

---
<!-- footer: Verse 2 -->
<!-- backgroundColor: deepskyblue -->

Humbly kneeling, sweet appealing—
’Twas the boy’s first uttered prayer—
When the pow’rs of sin assailing
Filled his soul with deep despair;

---

But undaunted, still he trusted
In his Heav’nly Father’s care,
But undaunted, still he trusted
In his Heav’nly Father’s care.

---
<!-- footer: Verse 3 -->
<!-- backgroundColor: deepskyblue -->

Suddenly a light descended,
Brighter far than noonday sun,
And a shining, glorious pillar
O’er him fell, around him shone,

---

While appeared two heav’nly beings,
God the Father and the Son,
While appeared two heav’nly beings,
God the Father and the Son.

---
<!-- footer: Verse 4 -->
<!-- backgroundColor: deepskyblue -->

“Joseph, this is my Beloved;
Hear him!” Oh, how sweet the word!
Joseph’s humble prayer was answered,
And he listened to the Lord.

---

Oh, what rapture filled his bosom,
For he saw the living God,
Oh, what rapture filled his bosom,
For he saw the living God.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->