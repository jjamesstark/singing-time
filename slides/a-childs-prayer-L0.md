---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'A Child’s Prayer (S12)'
footer: 'Level 0'
---
# A Child’s Prayer
## (Songbook 12)

---
<!-- header: A Child’s Prayer (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Heavenly Father, are you really there?

And do you hear and answer ev’ry child’s prayer?

Some say that heaven is far away,

But I feel it close around me as I pray.

---
<!-- header: A Child’s Prayer (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Heavenly Father, I remember now

Something that Jesus told disciples long ago:

“Suffer the children to come to me.”

Father, in prayer I’m coming now to thee.

---
<!-- header: A Child’s Prayer (Verse 2)-->
<!-- backgroundColor: LemonChiffon -->

Pray, he is there;

Speak, he is list’ning.

You are his child;

His love now surrounds you.

---
<!-- header: A Child’s Prayer (Verse 2)-->
<!-- backgroundColor: LemonChiffon -->

He hears your prayer;

He loves the children.

Of such is the kingdom, the kingdom of heav’n.

---
<!-- header: A Child’s Prayer -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->