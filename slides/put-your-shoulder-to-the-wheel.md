---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: Put Your Shoulder to the Wheel, (H252)'
footer: 'Level 0'
---

# Put Your Shoulder to the Wheel
## Hymnal 252

---
<!-- footer: (Verse 1) -->
<!-- backgroundColor: deepskyblue -->

The world has need of willing folks
Who wear the worker’s seal.
Come, help the good work move along;
Put your shoulder to the wheel.

---
<!-- footer: (Chorus) -->
<!-- backgroundColor: LemonChiffon -->

Put your shoulder to the wheel; push along,
Do your duty with a heart full of song,
We all have work; let no one shirk.
Put your shoulder to the wheel.

---
<!-- footer: (Verse 2) -->
<!-- backgroundColor: LightSalmon -->

The Church has need of helping hands,
And hearts that know and feel.
The work to do is here for you;
Put your shoulder to the wheel.

---
<!-- footer: (Chorus) -->
<!-- backgroundColor: LemonChiffon -->

Put your shoulder to the wheel; push along,
Do your duty with a heart full of song,
We all have work; let no one shirk.
Put your shoulder to the wheel.

---
<!-- footer: (Verse 3) -->
<!-- backgroundColor: MediumSpringGreen -->

Then don’t stand idly looking on;
The fight with sin is real.
It will be long but must go on;
Put your shoulder to the wheel.

---
<!-- footer: (Chorus) -->
<!-- backgroundColor: LemonChiffon -->

Put your shoulder to the wheel; push along,
Do your duty with a heart full of song,
We all have work; let no one shirk.
Put your shoulder to the wheel.

---
<!-- footer: (Verse 4) -->
<!-- backgroundColor: MediumSpringGreen -->

Then work and watch and fight and pray
With all your might and zeal.
Push ev’ry worthy work along;
Put your shoulder to the wheel.

---
<!-- footer: (Chorus) -->
<!-- backgroundColor: LemonChiffon -->

Put your shoulder to the wheel; push along,
Do your duty with a heart full of song,
We all have work; let no one shirk.
Put your shoulder to the wheel.

---
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->