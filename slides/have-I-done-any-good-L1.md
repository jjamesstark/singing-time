---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Have I Done Any Good (H223)'
footer: 'Level 1a'
---

# Have I Done Any Good 
## _(hymn 223)_

---
<!-- header: Have I Done Any Good (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Have I done any g____ in the world today?

Have I h_____ anyone in need?

Have I cheered up the sad and made s______ feel glad?

If not, I have f_____ indeed.

---

Has anyone’s burden been l______ today

Because I was w_______ to share?

Have the sick and the w____ been helped on their way?

When they n_____ my help was I there?

---

<!-- header: Have I Done Any Good (Chorus)-->
<!-- backgroundColor: LemonChiffon   -->

Then w___ up and do something more

Than dream of your m______ above.

Doing good is a p_______, a joy beyond measure,

A blessing of d___ and love.

----

<!-- header: Have I Done Any Good (Verse 2)-->
<!-- backgroundColor: LightPink   -->
 

There are chances for w___ all around just now,

O____________ right in our way.

Do not let them p___ by, saying, “Sometime I’ll try,”

But go and do s________ today.

---

’Tis n____ of man to work and to give;

Love’s labor has m____ alone.

Only he who does something helps o_____ to live.

To God each good w___ will be known.

---

<!-- header: Have I Done Any Good (Chorus)-->
<!-- backgroundColor: LemonChiffon   -->

Then wake up and do s________ more

Than d____ of your mansion above.

Doing good is a pleasure, a joy beyond m______,

A b_______ of duty and love.

---
<!-- header: Have I Done Any Good -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->