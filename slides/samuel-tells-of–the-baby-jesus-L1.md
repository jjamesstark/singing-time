---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Samuel Tells of the Baby Jesus (S36)'
footer: 'Level 1'
---

# Samuel Tells of the Baby Jesus
## Songbook 36

---
<!-- header: Samuel Tells of the Baby Jesus (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Said Samuel, “Within f___ years

A n____ will be as day,

And Baby Jesus will be b___

In a l___ far, far away.”

---
<!-- header: Samuel Tells of the Baby Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon -->

Hosanna! Hosanna!

Oh, let us g_____ sing.

How b______ that our Lord was born;

Let earth r______ her King!

---
<!-- header: Samuel Tells of the Baby Jesus (Verse 2)-->
<!-- backgroundColor: LightPink -->

Across the sea, in B________,

Lord Jesus came to e____

As Samuel had p_________,

And a_____ sang His birth.

---
<!-- header: Samuel Tells of the Baby Jesus (Chorus)-->
<!-- backgroundColor: LemonChiffon -->


Hosanna! H______!

Oh, let us g_____ s___.

How b______ that our Lord was b___;

Let e____ receive her King!

---
<!-- header: Samuel Tells of the Baby Jesus -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->