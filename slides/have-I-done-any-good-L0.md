---
marp: true
style: |
  {
    font-size: 1.6rem;
  }
header: 'Have I Done Any Good (H223)'
footer: 'Level 0'
---

# Have I Done Any Good 
## _(hymn 223)_

---

<!-- header: Have I Done Any Good (Verse 1)-->
<!-- backgroundColor: deepskyblue -->

Have I done any good in the world today?

Have I helped anyone in need?

Have I cheered up the sad and made someone feel glad?

If not, I have failed indeed.

---

Has anyone’s burden been lighter today

Because I was willing to share?

Have the sick and the weary been helped on their way?

When they needed my help was I there?

---

<!-- header: Have I Done Any Good (Chorus)-->
<!-- backgroundColor: LemonChiffon   -->

Then wake up and do something more

Than dream of your mansion above.

Doing good is a pleasure, a joy beyond measure,

A blessing of duty and love.

----

<!-- header: Have I Done Any Good (Verse 2)-->
<!-- backgroundColor: LightPink   -->
 

There are chances for work all around just now,

Opportunities right in our way.

Do not let them pass by, saying, “Sometime I’ll try,”

But go and do something today.

---

’Tis noble of man to work and to give;

Love’s labor has merit alone.

Only he who does something helps others to live.

To God each good work will be known.

---

<!-- header: Have I Done Any Good (Chorus)-->
<!-- backgroundColor: LemonChiffon   -->

Then wake up and do something more

Than dream of your mansion above.

Doing good is a pleasure, a joy beyond measure,

A blessing of duty and love.

---
<!-- header: Have I Done Any Good -->
<!-- footer: https://gitlab.com/jjamesstark/singing-time -->
<!-- backgroundColor: Black -->