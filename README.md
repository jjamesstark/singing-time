# Singing Time Resources

The following includes ideas, plans, slides, games, and other activities to aid in the programming of Singing Time

---

## Rehearsal Slides

These are html slide shows that can be used to learn the lyrics to songs and hymns we use in Singing Time. They get more difficult at each level, removing words, and then in the end, only show first letters or patterns.

- [A Child's Prayer (Songbook 12)](https://jjamesstark.gitlab.io/singing-time/a-childs-prayer-L0.html)
  - [Level 1: Missing one word per line](https://jjamesstark.gitlab.io/singing-time/a-childs-prayer-L1.html)
  - [Level 2: Missing multiple words per line](https://jjamesstark.gitlab.io/singing-time/a-childs-prayer-L2.html)
- [Christmas Bells (Songbook 54)](https://jjamesstark.gitlab.io/singing-time/christmas-bells.html)
- ["Give," Said the Little Stream(Songbook 236)](https://jjamesstark.gitlab.io/singing-time/give-said-the-little-stream-L1.html)
  - [Level 1: Missing some words](https://jjamesstark.gitlab.io/singing-time/give-said-the-little-stream-L1.html)
- [Have I Done Any Good (Hymnal 223)](https://jjamesstark.gitlab.io/singing-time/have-I-done-any-good-L0.html)
  - [Level 1: Missing one word per line](https://jjamesstark.gitlab.io/singing-time/have-I-done-any-good-L1.html)
- [He Sent His Son (Songbook 34)](https://jjamesstark.gitlab.io/singing-time/he-sent-his-son-L0.html)
  - [Level 1: Missing one word per line](https://jjamesstark.gitlab.io/singing-time/he-sent-his-son-L1.html)
  - [Level 2: Missing two words per line](https://jjamesstark.gitlab.io/singing-time/he-sent-his-son-L2.html)
  - [Level 3: Pattern preview](https://jjamesstark.gitlab.io/singing-time/he-sent-his-son-L3.html)
  - [Pattern](https://jjamesstark.gitlab.io/singing-time/he-sent-his-son-P.html)
- [I'm Trying to be Like Jesus (Songbook 78)](https://jjamesstark.gitlab.io/singing-time/im-trying-to-be-like-jesus-L0.html)
- [Joseph Smith's First Prayer (Hymnal 26)](https://jjamesstark.gitlab.io/singing-time/joseph-smiths-first-prayer.html)
- [Kindness Begins with Me (Songbook 145b)](https://jjamesstark.gitlab.io/singing-time/kindness-begins-with-me.html)
- [Put Your Shoulder to the Wheel (Hymnal 252)](https://jjamesstark.gitlab.io/singing-time/put-your-shoulder-to-the-wheel.html)
- [Roll Your Hands (Songbook 274)](https://jjamesstark.gitlab.io/singing-time/roll-your-hands.html)
- [Samuel Tells of the Baby Jesus (Songbook 36)](https://jjamesstark.gitlab.io/singing-time/samuel-tells-of–the-baby-jesus-L0.html)
  - [Level 1: Missing one word per line](https://jjamesstark.gitlab.io/singing-time/samuel-tells-of–the-baby-jesus-L1.html)
  - [Level 2: Missing multiple words per line](https://jjamesstark.gitlab.io/singing-time/samuel-tells-of–the-baby-jesus-L2.html)
- [Stars Were Gleaming (Songbook 37)](https://jjamesstark.gitlab.io/singing-time/stars-were-gleaming.html)
- [The Handcart Song (Songbook 220)](https://jjamesstark.gitlab.io/singing-time/the-handcart-song.html)
- [This Is My Beloved Son (Songbook 76)](https://jjamesstark.gitlab.io/singing-time/this-is-my-beloved-son-L0.html)
  - [Level 1: Missing some words](https://jjamesstark.gitlab.io/singing-time/this-is-my-beloved-son-L1.html)
- [We Are Different (Songbook 263)](https://jjamesstark.gitlab.io/singing-time/we-are-different-L0.html)
  - [Patttern](https://jjamesstark.gitlab.io/singing-time/we-are-different-P.html)
- [When We're Helping (Songbook 198b)](https://jjamesstark.gitlab.io/singing-time/when-were-helping-L0.html)
  - [Level 1: Missing Words](https://jjamesstark.gitlab.io/singing-time/when-were-helping-L1.html)
  - [First Letters Only](https://jjamesstark.gitlab.io/singing-time/when-were-helping-P.html)

---

## Games

### Secret Silent Singer

Get one volunteer to be the **Secret Silent Singer _Seeker_**, have them step into the hallway while you select another volunteer to be the **Secret Silent _Singer_**. The **Secret Silent _Singer_** will do their best to pretend to sing, while remaining completely silent. Once selected invite your first volunteer **Secret Silent Singer _Seeker_** back into the primary room and begin rehearsing. The **Secret Silent Singer _Seeker_** will walk through the rows trying to find the **Secret Silent _Singer_**.

### Hotter/Colder

One volunteer picks a small toy, which, after the volunteer is our of the room, the class hides. The volunteer returns to find the small toy, and the class sings louder as the volunteer gets closer and quiter as they get further away in order to help the volunteer find the hidden toy.
